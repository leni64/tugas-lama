<?php
$host      = "localhost";
$user      ="root";
$pass      ="";
$db        ="akademik";
$koneksi   = mysqli_connect($host,$user,$pass,$db);
if(!$koneksi){
    die("Tidak bisa terkoneksi ke database");
}
$nim    ="";
$nama   ="";
$alamat ="";
$prodi  ="";
$error  ="";
$sukses ="";

if(isset($_GET['op']))
{
  $op = $_GET['op'];
}
else
{
  $op="";
}
if($op == 'delete')
{
  $id =$_GET['id'];
  $sql1 ="delete from mahasiswa where id='$id'";
  $q1 =mysqli_query($koneksi,$sql1);
  if($q1)
  {
    $sukses ="Berhasil Menghapus data";
  }
   else
  {
    $error ="Gagal menghapus data";
  }
}
if($op =='edit')
{
  $id =$_GET['id'];
  $sql1 ="select *from mahasiswa where id='$id'";
  $q1 =mysqli_query($koneksi,$sql1);
  $r1 =mysqli_fetch_array($q1);
  $nim =$r1['nim'];
  $nama =$r1['nama'];
  $alamat =$r1['alamat'];
  $prodi =$r1['prodi'];

  if ($nim=='')
  {
    $error ="Data tidak ditemukan";
  }
}

if( isset($_POST['simpan'])){//untuk create
  $nim    = $_POST['nim'];
  $nama   = $_POST['nama'];
  $alamat    = $_POST['alamat'];
  $prodi    = $_POST['prodi'];

  if($nim && $nama && $alamat && $prodi){//untuk update
    if($op =='edit'){
      $sql1 ="update mahasiswa set nim ='$nim',nama='$nama',alamat='$alamat',prodi='$prodi' where id='$id'";
      $q1 =mysqli_query($koneksi,$sql1);
      if($q1){
        $sukses= "Data Berhasil di Update";
      }else{
        $error ="Data gagal di update";
      }
    }else{//untuk insert
    $sql1="insert into mahasiswa (nim,nama,alamat,prodi) values ('$nim','$nama','$alamat','$prodi')";
    $q1 =mysqli_query($koneksi,$sql1);
  if($q1){
    $sukses ="Berhasil Memasukkan Data Baru";
  } else{
    $error="Gagal Memasukkan data";
  } 
}
}else
{
  $error ="Silahkan masukkan data";
}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Data Mahasiswa</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <style>
        .mx-auto{
            width:800px
        }
        .card{
            margin-top:10px;
        }
    </style>
</head>
<body>
    <div class="mx-auto">
        <!-- Untuk memasukkan data -->
    <div class="card">
  <div class="card-header">
    Create / Edit data
  </div>
  <div class="card-body">
    <?php
    if($error){
      ?>
      <div class="alert alert-danger" role="alert">
  <?php echo $error ?>
</div>
      <?php
      header("refresh:5;url=index.php");//5 detik
    }
    ?>
     <?php
    if($sukses){
      ?>
      <div class="alert alert-success" role="alert">
  <?php echo $sukses ?>
</div>
      <?php
    header("refresh:5;url=index.php");
    }
    ?>
    <form action="" method="POST" >
  <div class="mb-3 row">
    <label for="nim" class="col-sm-2 col-form-label">NIM</label>
    <div class="col-sm-10">
      <input type="nim" class="form-control" id="nim" name="nim" value="<?php echo $nim?>">
    </div>
  </div>
  <div class="mb-3 row">
    <label for="nama" class="col-sm-2 col-form-label">NAMA</label>
    <div class="col-sm-10">
      <input type="nama" class="form-control" id="nama" name="nama" value="<?php echo $nama?>">
    </div>
  </div>
  <div class="mb-3 row">
    <label for="alamat" class="col-sm-2 col-form-label">ALAMAT</label>
    <div class="col-sm-10">
      <input type="alamat" class="form-control" id="alamat" name="alamat" value="<?php echo $alamat?>">
    </div>
  </div>
  <div class="mb-3 row">
    <label for="prodi" class="col-sm-2 col-form-label">PRODI</label>
    <div class="col-sm-10">
      <select class="form-control" name="prodi" id="prodi">
        <option value="">-Pilih Fakultas</option>
        <option value="mif" <?php if($prodi == "Manajemen Informatika") echo "selected" ?>>Manajemen Informatika</option>
        <option value="tmm" <?php if($prodi == "Teknik Multimedia") echo "selected" ?>>Teknik Multimedia</option>
        <option value="akp" <?php if($prodi == "Akuntansi Keuangan Perusahaan") echo "selected"?>>Akuntansi Keuangan Perusahaan</option>
        <option value="tm" <?php if($prodi == "Teknik Mesin") echo "selected"?>>Teknik Mesin</option>
        <option value="tmp" <?php if($prodi == "Teknik Mesin Pertanian") echo "selected"?>>Teknik Mesin Pertanian</option>
        <option value="mbp" <?php if($prodi == "Manajemen Bisnis Pariwisata") echo "selected"?>>Manajemen Bisnis Pariwisata</option>
        <option value="ag"<?php if($prodi == "Agrobisnis") echo "selected"?>>Agrobisnis</option>
        <option value="apk"<?php if($prodi == "Agroindustri Perikanan dan kelautan") echo "selected"?>>Agroindustri Perikanan dan Kelautan</option>
        <option value="aip" <?php if($prodi == "Agroindustri Pangan") echo "selected"?>>Agroindustri Pangan</option>

      </select> 
    </div>
    <div class="col-12">
      <input type="submit" name="simpan" value="Simpan Data" class="btn btn-primary">
    </div>
  </div>
    </form>
  </div> 
</div>
<!--untuk mengeluarkan data-->
<div class="card">
  <div class="card-header text-black bg secondary">
    Data Mahasiswa
  </div>
  <div class="card-body">
    <table class="table">
    <thead>
        <tr>
          <th scope="col"> No.</th>
          <th scope="col"> NIM</th>
          <th scope="col"> NAMA</th>
          <th scope="col"> ALAMAT</th>
          <th scope="col"> PRODI</th>
          <th scope="col"> AKSI</th>
        </tr>
        <tbody>
          <?php
          $sql2 ="select * from mahasiswa order by id desc";
          $q2 =mysqli_query($koneksi,$sql2);
          $urut = 1;

          while($r2 =mysqli_fetch_array($q2)){
            $id = $r2['id'];
            $nim =$r2['nim'];
            $nama   =$r2['nama'];
            $alamat =$r2['alamat'];
            $prodi  =$r2['prodi'];
          
          ?>
          <tr>
            <th scope="row"><?php echo $urut++ ?></th>
            <td scope="row"><?php echo $nim ?></td>
            <td scope="row"><?php echo $nama ?></td>
            <td scope="row"><?php echo $alamat ?></td>
            <td scope="row"><?php echo $prodi ?></td>
            <td scope="row">
              <a href="index.php? op=edit&id=<?php echo $id?>"><button type="button" class="btn btn-warning">Edit</button></a>
              <a href="index.php?op=delete&id=<?php echo $id?>" onclick="return confirm('Yakin ingin mendelete data?')"><button type="button" class="btn btn-danger">Delete</button></a>

            </td>
          </tr>
          <?php

          }
          ?>
        </tbody>
    </thead>
    </table>
  </div>
</div>
    </div>
</body>
</html>